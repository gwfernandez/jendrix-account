package com.jendrix.account;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class TestCrypt {

	
	public static void main(String[] args) {
		BCryptPasswordEncoder encrypt = new BCryptPasswordEncoder();
		System.out.println(encrypt.encode("admin"));
	}
	
}
