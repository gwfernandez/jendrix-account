insert into aplicacion (aplicacion_id, codigo, nombre,  descripcion, fecha_creacion, vigencia_desde) values (1, 'JENDRIX_ACCOUNT', 'Jendrix Account', '?', current_timestamp, current_timestamp);
insert into aplicacion_rol (aplicacion_rol_id, aplicacion_id, rol, fecha_creacion, vigencia_desde) values (1, 1, 'JX_ACCOUNT_ADMIN', current_timestamp, current_timestamp);

insert into aplicacion (aplicacion_id, codigo, nombre,  descripcion, fecha_creacion, vigencia_desde) values (2, 'JENDRIX_EXPERIENCE', 'Jendrix Experience', '?', current_timestamp, current_timestamp);
insert into aplicacion_rol (aplicacion_rol_id, aplicacion_id, rol, fecha_creacion, vigencia_desde) values (2, 2, 'JX_EXPERIENCE_ADMIN', current_timestamp, current_timestamp);

insert into aplicacion (aplicacion_id, codigo, nombre,  descripcion, fecha_creacion, vigencia_desde) values (3, 'JENDRIX_GYMNASTIC', 'Jendrix Gymnastic', '?', current_timestamp, current_timestamp);
insert into aplicacion_rol (aplicacion_rol_id, aplicacion_id, rol, fecha_creacion, vigencia_desde) values (3, 3, 'JX_GYMNASTIC_ADMIN', current_timestamp, current_timestamp);

insert into cuenta (cuenta_id, nombre, password, alias) values (1, 'admin', '$2a$10$L3DyiXDoE8uuDol9izqQzeqIdPHgN8ceBd..V8c5CsaQ2i9BPxn1e', 'admin');

insert into cuenta_rol (cuenta_rol_id, cuenta_id, aplicacion_rol_id) values (1, 1 , 1);
