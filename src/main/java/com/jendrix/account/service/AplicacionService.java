package com.jendrix.account.service;

import java.util.List;

import org.hibernate.service.spi.ServiceException;

import com.jendrix.account.model.AplicacionModel;
import com.jendrix.common.exception.ServiceRestrictionException;

public interface AplicacionService {

	public List<AplicacionModel> getListAll() throws ServiceException;

	public AplicacionModel getAplicacionById(Long id) throws ServiceException;

	public AplicacionModel save(AplicacionModel aplicacionModel) throws ServiceRestrictionException, ServiceException;

	public void delete(Long id) throws ServiceRestrictionException, ServiceException;

}
