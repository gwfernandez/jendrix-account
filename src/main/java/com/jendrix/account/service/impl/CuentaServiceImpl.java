package com.jendrix.account.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.jendrix.account.entity.Cuenta;
import com.jendrix.account.entity.CuentaRol;
import com.jendrix.account.repository.CuentaRepository;
import com.jendrix.account.service.CuentaService;

@Service ("cuentaServiceImpl")
public class CuentaServiceImpl implements UserDetailsService, CuentaService {

	@Autowired
	@Qualifier("cuentaRepository")
	private CuentaRepository cuentaRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = null;
		Cuenta cuenta = this.cuentaRepository.findByNombre(username);
		if (cuenta != null) {
			List<GrantedAuthority> authorities = buildAuthorities(cuenta.getRoles());
			user = buildUser(cuenta, authorities);
		}

		return user;
	}

	private User buildUser(Cuenta cuenta, List<GrantedAuthority> authorities) {
		User user = null;
		if (cuenta != null) {
			boolean enabled = true;
			boolean accountNonExpired = true;
			boolean credentialsNonExpired = true;
			boolean accountNonLocked = true;
			user = new User(cuenta.getNombre(), cuenta.getPassword(), enabled, accountNonExpired, credentialsNonExpired,
					accountNonLocked, authorities);
		}
		return user;
	}

	private List<GrantedAuthority> buildAuthorities(Set<CuentaRol> roles) {
		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		if (roles != null) {
			for (CuentaRol cuentaRol : roles) {
				authorities.add(new SimpleGrantedAuthority(cuentaRol.getAplicacionRol().getRol()));
			}
		}
		return new ArrayList<GrantedAuthority>(authorities);
	}
}
