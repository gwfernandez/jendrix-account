package com.jendrix.account.service.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.jendrix.account.component.converter.AplicacionConverter;
import com.jendrix.account.entity.Aplicacion;
import com.jendrix.account.model.AplicacionModel;
import com.jendrix.account.repository.AplicacionRepository;
import com.jendrix.account.service.AplicacionService;
import com.jendrix.common.exception.ServiceRestrictionException;

@Service("aplicacionServiceImpl")
public class AplicacionServiceImpl implements AplicacionService {

	private static final Log LOG = LogFactory.getLog(AplicacionServiceImpl.class);

	// TODO: revisar herencia BaseService
	// TODO: revisar manejo de excepciones
	// TODO: agregar log en consola

	@Autowired
	@Qualifier("aplicacionRepository")
	private AplicacionRepository aplicacionRepository;

	@Autowired
	@Qualifier("aplicacionConverter")
	private AplicacionConverter aplicacionConverter;

	@Override
	public List<AplicacionModel> getListAll() throws ServiceException {
		try {
			List<Aplicacion> aplicacionList = this.aplicacionRepository.findAll();
			return this.aplicacionConverter.toModelList(aplicacionList);
		} catch (Exception e) {
			LOG.error("Error al recuperar la lista de aplicaciones!", e);
			throw new ServiceException("Error al recuperar la lista de aplicaciones!");
		}
	}

	@Override
	public AplicacionModel getAplicacionById(Long id) throws ServiceException {
		try {
			AplicacionModel aplicacionModel = null;
			if (id != null) {
				Aplicacion aplicacion = this.aplicacionRepository.findById(id);
				aplicacionModel = this.aplicacionConverter.toModel(aplicacion);
			}
			return aplicacionModel;
		} catch (Exception e) {
			LOG.error("Error al recuperar la aplicacion ID: " + id, e);
			throw new ServiceException("Error al recuperar la aplicacion ID: " + id);
		}
	}

	@Override
	public AplicacionModel save(AplicacionModel aplicacionModel) throws ServiceRestrictionException, ServiceException {
		String param = null;
		try {
			param = aplicacionModel != null ? aplicacionModel.toString() : "[null]";
			validateSave(aplicacionModel);
			Aplicacion newAplicacion = this.aplicacionConverter.toEntity(aplicacionModel);
			newAplicacion = this.aplicacionRepository.save(newAplicacion);
			return this.aplicacionConverter.toModel(newAplicacion);
		} catch (ServiceRestrictionException sre) {
			throw sre;
		} catch (Exception e) {
			LOG.error("Error al guardar la aplicacion. Parámetros: " + param, e);
			throw new ServiceException("Error al guardar la aplicación.");
		}
	}

	private void validateSave(AplicacionModel aplicacionModel) throws ServiceRestrictionException, ServiceException {
		// TODO Auto-generated method stub
	}

	@Override
	public void delete(Long id) throws ServiceRestrictionException, ServiceException {
		try {
			if (id != null) {
				Aplicacion aplicacion = this.aplicacionRepository.findById(id);
				validateDelete(aplicacion);
				aplicacionRepository.delete(id);
			}
		} catch (ServiceRestrictionException sre) {
			throw sre;
		} catch (Exception e) {
			LOG.error("Error al eliminar la aplicacion ID: " + id, e);
			throw new ServiceException("Error al eliminar la aplicacion ID: " + id);
		}
	}

	private void validateDelete(Aplicacion aplicacion) throws ServiceRestrictionException, ServiceException {
		if (aplicacion == null) {
			throw new ServiceRestrictionException("No se entonctro el registro a eliminar");
		}
	}
}
