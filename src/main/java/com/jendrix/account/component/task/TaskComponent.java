package com.jendrix.account.component.task;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

@Component("taskComponent")
public class TaskComponent {

	private static final Log LOG = LogFactory.getLog(TaskComponent.class);

//	@Scheduled(fixedDelay = 5000)
	public void doTask() {
		LOG.info("Time: " + new Date());
	}

}
