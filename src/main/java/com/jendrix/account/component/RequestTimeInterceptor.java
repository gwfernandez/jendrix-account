package com.jendrix.account.component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.jendrix.account.repository.LogRepository;

@Component("RequestTimeInterceptor")
public class RequestTimeInterceptor extends HandlerInterceptorAdapter {

	private static final Log log = LogFactory.getLog(RequestTimeInterceptor.class);

	@Autowired
	@Qualifier("logRepository")
	private LogRepository logRepository;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		request.setAttribute("startTime", System.currentTimeMillis());
		return true;
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {

		if (request != null && request.getClass().getSimpleName().equals("ApplicationHttpRequest")) {
			// TODO revisar porque logguea 3 varias veces error
			log.debug("este metodo no se procesa");
			return;
		}

		long startTime = (long) request.getAttribute("startTime");

		String url = request.getRequestURL().toString();
		// String username = "-";
		// String datos = "prueba";
		// String detalle = "-";
		//
		// Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		// if (auth != null && auth.isAuthenticated()) {
		// username = auth.getName();
		// detalle = auth.getDetails().toString();
		// }
		//
		// com.jendrix.account.entity.Log l = new
		// com.jendrix.account.entity.Log(detalle, datos, url, username);
		// logRepository.save(l);

		log.info("URL: " + url + " -- LOAD IN: " + (System.currentTimeMillis() - startTime + "ms"));
	}

}
