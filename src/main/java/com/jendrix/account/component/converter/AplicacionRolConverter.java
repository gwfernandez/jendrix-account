package com.jendrix.account.component.converter;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Component;

import com.jendrix.account.entity.Aplicacion;
import com.jendrix.account.entity.AplicacionRol;
import com.jendrix.account.model.AplicacionRolModel;

@Component("aplicacionRolConverter")
public class AplicacionRolConverter {

	public AplicacionRol toEntity(AplicacionRolModel model) {
		AplicacionRol entity = null;
		if (model != null) {
			entity = new AplicacionRol();
			entity.setId(model.getId());
			entity.setAplicacion(new Aplicacion(model.getAplicacionModel().getId()));
			entity.setRol(model.getRol());
			entity.setDescripcion(model.getDescripcion());
			entity.setFechaCreacion(model.getFechaCreacion());
			entity.setVigenciaDesde(model.getVigenciaDesde());
			entity.setVigenciaHasta(model.getVigenciaHasta());
		}
		return entity;
	}

	public AplicacionRolModel toModel(AplicacionRol entity) {
		AplicacionRolModel model = null;
		if (entity != null) {
			model = new AplicacionRolModel();
			model.setId(entity.getId());
			model.getAplicacionModel().setId(entity.getAplicacion().getId());
			model.setRol(entity.getRol());
			model.setDescripcion(entity.getDescripcion());
			model.setFechaCreacion(entity.getFechaCreacion());
			model.setVigenciaDesde(entity.getVigenciaDesde());
			model.setVigenciaHasta(entity.getVigenciaHasta());
		}
		return model;
	}

	public Set<AplicacionRolModel> toModelList(Set<AplicacionRol> roles) {
		Set<AplicacionRolModel> aplicacionRolModelList = null;
		if (roles != null) {
			aplicacionRolModelList = new HashSet<AplicacionRolModel>();
			for (AplicacionRol entity : roles) {
				aplicacionRolModelList.add(toModel(entity));
			}
		}
		return aplicacionRolModelList;
	}

	public Set<AplicacionRol> toEntityList(Set<AplicacionRolModel> modelList) {
		Set<AplicacionRol> aplicacionRolList = null;
		if (modelList != null) {
			aplicacionRolList = new HashSet<AplicacionRol>();
			for (AplicacionRolModel model : modelList) {
				aplicacionRolList.add(toEntity(model));
			}
		}
		return aplicacionRolList;
	}

}
