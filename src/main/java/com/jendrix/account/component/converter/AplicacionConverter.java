package com.jendrix.account.component.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jendrix.account.entity.Aplicacion;
import com.jendrix.account.model.AplicacionModel;

@Component("aplicacionConverter")
public class AplicacionConverter {

	@Autowired
	private AplicacionRolConverter aplicacionRolConverter;

	public Aplicacion toEntity(AplicacionModel model) {
		Aplicacion entity = null;
		if (model != null) {
			entity = new Aplicacion();
			entity.setId(model.getId());
			entity.setNombre(model.getNombre());
			entity.setDescripcion(model.getDescripcion());
			entity.setCodigo(model.getCodigo());
			entity.setFechaCreacion(model.getFechaCreacion());
			entity.setVigenciaDesde(model.getVigenciaDesde());
			entity.setVigenciaHasta(model.getVigenciaHasta());
			entity.setRoles(aplicacionRolConverter.toEntityList(model.getRoles()));
		}

		return entity;
	}

	public AplicacionModel toModel(Aplicacion entity) {
		AplicacionModel model = null;
		if (entity != null) {
			model = new AplicacionModel();
			model.setId(entity.getId());
			model.setNombre(entity.getNombre());
			model.setDescripcion(entity.getDescripcion());
			model.setCodigo(entity.getCodigo());
			model.setFechaCreacion(entity.getFechaCreacion());
			model.setVigenciaDesde(entity.getVigenciaDesde());
			model.setVigenciaHasta(entity.getVigenciaHasta());
			model.setRoles(aplicacionRolConverter.toModelList(entity.getRoles()));
		}
		return model;
	}

	public List<AplicacionModel> toModelList(List<Aplicacion> aplicacionList) {
		List<AplicacionModel> aplicacionModelList = null;
		if (aplicacionList != null) {
			aplicacionModelList = new ArrayList<>();
			for (Aplicacion entity : aplicacionList) {
				aplicacionModelList.add(toModel(entity));
			}
		}
		return aplicacionModelList;
	}
}
