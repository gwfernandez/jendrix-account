package com.jendrix.account.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public final class AplicacionRolModel {

	private Long id;
	private AplicacionModel aplicacionModel;
	private String rol;
	private String descripcion;
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date fechaCreacion;
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date vigenciaDesde;
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date vigenciaHasta;

	/**
	 * Constructor default.
	 */
	public AplicacionRolModel() {
		super();
	}

	// @Override
	public final Long getId() {
		return id;
	}

	// @Override
	public final void setId(Long id) {
		this.id = id;
	}

	public AplicacionModel getAplicacionModel() {
		if (aplicacionModel == null) {
			aplicacionModel = new AplicacionModel();
		}
		return aplicacionModel;
	}

	public void setAplicacionModel(AplicacionModel aplicacionModel) {
		this.aplicacionModel = aplicacionModel;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getVigenciaDesde() {
		return vigenciaDesde;
	}

	public void setVigenciaDesde(Date vigenciaDesde) {
		this.vigenciaDesde = vigenciaDesde;
	}

	public Date getVigenciaHasta() {
		return vigenciaHasta;
	}

	public void setVigenciaHasta(Date vigenciaHasta) {
		this.vigenciaHasta = vigenciaHasta;
	}

	@Override
	public String toString() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String vigenciaDesdeString = (vigenciaDesde == null ? "" : sdf.format(vigenciaDesde));
		String vigenciaHastaString = (vigenciaHasta == null ? "" : sdf.format(vigenciaHasta));
		return "AplicacionRolModel [id=" + id + ", rol=" + rol + ", descripcion=" + descripcion + ", vigenciaDesde="
				+ vigenciaDesdeString + ", vigenciaHasta=" + vigenciaHastaString + "]";
	}
	
	
}