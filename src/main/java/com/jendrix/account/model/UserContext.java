package com.jendrix.account.model;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import com.jendrix.common.util.Checker;

@Component
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS, value = "session")
public class UserContext {

	private User user;
	private Date loginTime;
	private Date logoutTime;

	protected UserContext(User user) {
		super();
		this.user = user;
		this.loginTime = new Date();
	}

	
	private void loadUser() {
		this.user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}
	
	public User getUser() {
		if (user == null) {
			loadUser();
		}
		return user;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public Date getLogoutTime() {
		return logoutTime;
	}

	public final String getUsername() {
		String username = "";
		if (getUser() != null && !Checker.isEmpty(getUser().getUsername())) {
			username = getUser().getUsername();
		}
		return username;
	}

}
