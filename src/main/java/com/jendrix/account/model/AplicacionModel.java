package com.jendrix.account.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

public class AplicacionModel {

	private Long id;

	@NotEmpty
	@Size(max = 100)	
	private String nombre;

	@NotEmpty
	@Size(max = 500)
	private String descripcion;

	@NotEmpty
	@Size(max = 50)
	private String codigo;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date fechaCreacion;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date vigenciaDesde;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date vigenciaHasta;

	private Set<AplicacionRolModel> roles;

	public AplicacionModel() {
		super();
	}

	public AplicacionModel(Long id, String nombre, String descripcion, String codigo, Date fechaCreacion,
			Date vigenciaDesde, Date vigenciaHasta) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.codigo = codigo;
		this.fechaCreacion = fechaCreacion;
		this.vigenciaDesde = vigenciaDesde;
		this.vigenciaHasta = vigenciaHasta;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getVigenciaDesde() {
		return vigenciaDesde;
	}

	public void setVigenciaDesde(Date vigenciaDesde) {
		this.vigenciaDesde = vigenciaDesde;
	}

	public Date getVigenciaHasta() {
		return vigenciaHasta;
	}

	public void setVigenciaHasta(Date vigenciaHasta) {
		this.vigenciaHasta = vigenciaHasta;
	}

	public Set<AplicacionRolModel> getRoles() {
		return roles;
	}

	public void setRoles(Set<AplicacionRolModel> roles) {
		this.roles = roles;
	}

	@Override
	public String toString() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String vigenciaDesdeString = (vigenciaDesde == null ? "" : sdf.format(vigenciaDesde));
		String vigenciaHastaString = (vigenciaHasta == null ? "" : sdf.format(vigenciaHasta));

		return "AplicacionModel [id=" + id + ", nombre=" + nombre + ", descripcion=" + descripcion + ", codigo="
				+ codigo + ", fechaAlta=" + vigenciaDesdeString + ", fechaBaja=" + vigenciaHastaString + "]";
	}

}