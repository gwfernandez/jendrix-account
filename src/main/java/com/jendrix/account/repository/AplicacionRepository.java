package com.jendrix.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jendrix.account.entity.Aplicacion;

@Repository("aplicacionRepository")
public interface AplicacionRepository extends JpaRepository<Aplicacion, Long> {

	public abstract Aplicacion findById(Long id);

}
