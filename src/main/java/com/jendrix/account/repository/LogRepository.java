package com.jendrix.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jendrix.account.entity.Log;

@Repository("logRepository")
public interface LogRepository extends JpaRepository<Log, Long> {

}
