package com.jendrix.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jendrix.account.entity.Cuenta;

@Repository("cuentaRepository")
public interface CuentaRepository extends JpaRepository<Cuenta, Long> {

	public abstract Cuenta findByNombre(String nombre);

}
