package com.jendrix.account.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "activacion")
public final class Activacion {

	@Id
	@SequenceGenerator(name = "activacionSEQ", sequenceName = "activacion_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "activacionSEQ")
	@Column(name = "activacion_id", unique = true)
	private Long activacionId;

	@Column(name = "email", length = 100)
	private String email;

	@Column(name = "codigo_activacion", length = 100)
	private String codigoActivacion;

	@Column(name = "fecha_desde")
	private Date fechaDesde;

	@Column(name = "fecha_hasta")
	private Date fechaHasta;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "rol_id")
	private AplicacionRol rol;

	// @Override
	public final Long getId() {
		return activacionId;
	}

	// @Override
	public final void setId(Long id) {
		this.activacionId = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCodigoActivacion() {
		return codigoActivacion;
	}

	public void setCodigoActivacion(String codigoActivacion) {
		this.codigoActivacion = codigoActivacion;
	}

	public Date getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public Date getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public AplicacionRol getRol() {
		return rol;
	}

	public void setRol(AplicacionRol rol) {
		this.rol = rol;
	}

}