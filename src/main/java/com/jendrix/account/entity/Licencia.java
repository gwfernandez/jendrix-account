package com.jendrix.account.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "licencia")
public final class Licencia {

	@Id
	@SequenceGenerator(name = "licenciaSEQ", sequenceName = "licencia_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "licenciaSEQ")
	@Column(name = "licencia_id", unique = true)
	private Long licenciaId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cuenta_id")
	private Cuenta cuenta;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "aplicacion_id")
	private Aplicacion aplicacion;

	@Column(name = "fecha_desde")
	private Date fechaDesde;

	@Column(name = "fecha_hasta")
	private Date fechaHasta;

	// @Override
	public final Long getId() {
		return licenciaId;
	}

	// @Override
	public final void setId(Long id) {
		this.licenciaId = id;
	}

	public Cuenta getCuenta() {
		return cuenta;
	}

	public void setCuenta(Cuenta cuenta) {
		this.cuenta = cuenta;
	}

	public Aplicacion getAplicacion() {
		return aplicacion;
	}

	public void setAplicacion(Aplicacion aplicacion) {
		this.aplicacion = aplicacion;
	}

	public Date getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public Date getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

}