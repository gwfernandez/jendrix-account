package com.jendrix.account.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "aplicacion_rol", uniqueConstraints = @UniqueConstraint(columnNames = { "aplicacion_id", "rol" }))
public final class AplicacionRol {

	@Id
	@SequenceGenerator(name = "aplicacionRolSEQ", sequenceName = "aplicacion_rol_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "aplicacionRolSEQ")
	@Column(name = "aplicacion_rol_id", unique = true)
	private Long aplicacionRolId;
	// TODO: definir los nombre de los ID de las entidades

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "aplicacion_id", nullable = false)
	private Aplicacion aplicacion;

	@Column(name = "rol", length = 50, nullable = false, unique = true)
	private String rol;

	@Column(name = "descripcion", length = 500)
	private String descripcion;

	@Column(name = "fecha_creacion")
	private Date fechaCreacion;

	@Column(name = "vigencia_desde")
	private Date vigenciaDesde;

	@Column(name = "vigencia_hasta")
	private Date vigenciaHasta;

	/**
	 * Constructor default.
	 */
	public AplicacionRol() {
		super();
	}

	// @Override
	public final Long getId() {
		return aplicacionRolId;
	}

	// @Override
	public final void setId(Long id) {
		this.aplicacionRolId = id;
	}

	public Aplicacion getAplicacion() {
		return aplicacion;
	}

	public void setAplicacion(Aplicacion aplicacion) {
		this.aplicacion = aplicacion;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getVigenciaDesde() {
		return vigenciaDesde;
	}

	public void setVigenciaDesde(Date vigenciaDesde) {
		this.vigenciaDesde = vigenciaDesde;
	}

	public Date getVigenciaHasta() {
		return vigenciaHasta;
	}

	public void setVigenciaHasta(Date vigenciaHasta) {
		this.vigenciaHasta = vigenciaHasta;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aplicacion == null) ? 0 : aplicacion.hashCode());
		result = prime * result + ((rol == null) ? 0 : rol.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AplicacionRol other = (AplicacionRol) obj;
		if (aplicacion == null) {
			if (other.aplicacion != null)
				return false;
		} else if (!aplicacion.equals(other.aplicacion))
			return false;
		if (rol == null) {
			if (other.rol != null)
				return false;
		} else if (!rol.equals(other.rol))
			return false;
		return true;
	}
}