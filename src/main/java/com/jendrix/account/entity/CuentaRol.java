package com.jendrix.account.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "cuenta_rol", uniqueConstraints = @UniqueConstraint(columnNames = { "cuenta_id", "aplicacion_rol_id" }))
public class CuentaRol {

	@Id
	@SequenceGenerator(name = "cuentaRolSEQ", sequenceName = "cuenta_rol_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cuentaRolSEQ")
	@Column(name = "cuenta_rol_id", unique = true)
	private Long cuentaRolId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cuenta_id")
	private Cuenta cuenta;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "aplicacion_rol_id")
	private AplicacionRol aplicacionRol;

	/**
	 * Constructor default
	 */
	public CuentaRol() {
		super();
	}

	public Long getId() {
		return cuentaRolId;
	}

	public void setId(Long cuentaRolId) {
		this.cuentaRolId = cuentaRolId;
	}

	public Cuenta getCuenta() {
		return cuenta;
	}

	public void setCuenta(Cuenta cuenta) {
		this.cuenta = cuenta;
	}

	public AplicacionRol getAplicacionRol() {
		return aplicacionRol;
	}

	public void setAplicacionRol(AplicacionRol aplicacionRol) {
		this.aplicacionRol = aplicacionRol;
	}

}
