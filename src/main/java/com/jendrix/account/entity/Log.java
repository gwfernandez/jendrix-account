package com.jendrix.account.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "log")
public class Log {

	@Id
	@SequenceGenerator(name = "logSEQ", sequenceName = "log_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "logSEQ")
	@Column(name = "log_id", nullable = false)
	private Long id;

	@Column(name = "fecha", nullable = false)
	private Date fecha;

	@Column(name = "detalle", nullable = false, length = 500)
	private String detalle;

	// TODO: ver como guardar textos largos. formato json
	@Column(name = "datos", nullable = true, length = 1024)
	private String datos;

	@Column(name = "url", nullable = true, length = 256)
	private String url;

	@Column(name = "username", nullable = true, length = 100)
	private String username;

	public Log(String detalle, String datos, String url, String username) {
		super();
		this.fecha = new Date();
		this.detalle = detalle;
		this.datos = datos;
		this.url = url;
		this.username = username;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public String getDatos() {
		return datos;
	}

	public void setDatos(String datos) {
		this.datos = datos;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
