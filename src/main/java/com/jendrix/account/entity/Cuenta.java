package com.jendrix.account.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "cuenta")
public final class Cuenta {

	@Id
	@SequenceGenerator(name = "cuentaSEQ", sequenceName = "cuenta_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cuentaSEQ")
	@Column(name = "cuenta_id", unique = true)
	private Long cuentaId;

	@Column(name = "nombre", length = 100, unique = true, nullable = false)
	private String nombre;

	@Column(name = "password", length = 60, nullable = false)
	private String password;

	@Column(name = "alias", length = 100)
	private String alias;

	@Column(name = "fecha_alta")
	private Date fechaAlta;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "cuenta", orphanRemoval = true)
	private Set<CuentaRol> roles;

	public final Long getId() {
		return cuentaId;
	}

	public final void setId(Long id) {
		this.cuentaId = id;
	}

	public final String getNombre() {
		return nombre;
	}

	public final void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public final String getPassword() {
		return password;
	}

	public final void setPassword(String password) {
		this.password = password;
	}

	public final String getAlias() {
		return alias;
	}

	public final void setAlias(String alias) {
		this.alias = alias;
	}

	public final Date getFechaAlta() {
		return fechaAlta;
	}

	public final void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Set<CuentaRol> getRoles() {
		return roles;
	}

	public void setRoles(Set<CuentaRol> roles) {
		this.roles = roles;
	}

}