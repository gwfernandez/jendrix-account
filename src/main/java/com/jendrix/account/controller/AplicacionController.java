package com.jendrix.account.controller;

import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.jendrix.account.model.AplicacionModel;
import com.jendrix.account.service.AplicacionService;
import com.jendrix.common.exception.ServiceRestrictionException;

@Controller
@RequestMapping("/aplicaciones")
@SessionAttributes({ "aplicacion", "view" })
public class AplicacionController {

	private static final Log LOG = LogFactory.getLog(AplicacionController.class);

	@Autowired
	@Qualifier("aplicacionServiceImpl")
	private AplicacionService aplicacionService;
	
	@GetMapping("/main")
	public String main(Model model) {
		LOG.info("METHOD INVOKED FORM UI: main()");
		try {
			model.addAttribute("aplicacionList", this.aplicacionService.getListAll());
			model.addAttribute("view", ViewNameConstant.APLICACION_MAIN);
		} catch (ServiceException se) {
			model.addAttribute("error", "En este momento no se puede realizar la operación solicitada");
		} catch (Exception e) {
			LOG.error("Error al cargar la pagina principal de aplicaciones.", e);
			model.addAttribute("error", "No se puede cargar la página principal de aplicaciones.");
		}

		return ViewNameConstant.APLICACION_MAIN.getName();
	}

	// @PreAuthorize("hasRole('JX_ACCOUNT_ADMIN')")
	@GetMapping("/add")
	public String add(Model model) {
		LOG.info("METHOD INVOKED FORM UI: add()");
		String returnPage = ViewNameConstant.APLICACION_MAIN.getName();
		try {
			model.addAttribute("aplicacion", new AplicacionModel());
			returnPage = ViewNameConstant.APLICACION_EDIT.getName();
		} catch (Exception e) {
			LOG.error("Error al invocar el metodo add.", e);
			model.addAttribute("error", "En este momento no se puede realizar la operación solicitada");
		}
		return returnPage;
	}

	@GetMapping("/edit")
	public String edit(@RequestParam(name = "id", required = true) Long id, Model model) {
		LOG.info("METHOD INVOKED FORM UI: edit() -- PARAMS: id=" + id);
		String returnPage = ViewNameConstant.APLICACION_MAIN.getName();
		try {
			AplicacionModel aplicacionModel = this.aplicacionService.getAplicacionById(id);
			if (aplicacionModel != null) {
				model.addAttribute("aplicacion", aplicacionModel);
				returnPage = ViewNameConstant.APLICACION_EDIT.getName();
			} else {
				LOG.warn("La aplicación solicitada no existe. ID: " + id);
				model.addAttribute("warning", "La aplicación solicitada no existe");
				// returnPage = "redirect:/aplicaciones/main";
			}

		} catch (ServiceException se) {
			model.addAttribute("error", "En este momento no se puede realizar la operación solicitada");
		} catch (Exception e) {
			LOG.error("Error al recuperar la aplicacion ID: " + id, e);
			model.addAttribute("error", "En este momento no se puede realizar la operación solicitada");
		}
		return returnPage;
	}

	@GetMapping("/delete")
	public String delete(@RequestParam(name = "id", required = true) Long id, Model model) {
		LOG.info("METHOD INVOKED FORM UI: delete() -- PARAMS: id=" + id);
		try {
			this.aplicacionService.delete(id);
			model.addAttribute("aplicacionList", this.aplicacionService.getListAll());
			model.addAttribute("success", "Se elimino correctamente la aplicación");
		} catch (ServiceRestrictionException sre) {
			model.addAttribute("valid", sre.getMessages());
		} catch (ServiceException se) {
			model.addAttribute("error", "En este momento no se puede realizar la operación solicitada");
		} catch (Exception e) {
			LOG.error("Error al eliminar la aplicacion ID: " + id, e);
			model.addAttribute("error", "En este momento no se puede realizar la operación solicitada");
		}
		return ViewNameConstant.APLICACION_MAIN.getName();
	}

	@PostMapping("/save")
	public String save(@Valid @ModelAttribute("aplicacion") AplicacionModel aplicacion, BindingResult result, Model model, SessionStatus status) {
		LOG.info("METHOD INVOKED FORM UI: save() -- PARAMS: " + aplicacion.toString());
		String returnPage = ViewNameConstant.APLICACION_EDIT.getName();
		try {
			if (!result.hasErrors()) {
				this.aplicacionService.save(aplicacion);
				model.addAttribute("status", "OK");
				model.addAttribute("success", "Se agrego correctamente la aplicación '" + aplicacion.getNombre() + "'");

				status.setComplete();

				returnPage = "redirect:/aplicaciones/main";
			}
		} catch (ServiceRestrictionException sre) {
			model.addAttribute("valid", sre.getMessages());
		} catch (ServiceException se) {
			model.addAttribute("status", "ERROR");
			model.addAttribute("error", "En este momento no se puede realizar la operación solicitada");
		} catch (Exception e) {
			LOG.error("Error al guardar aplicación", e);
			model.addAttribute("status", "ERROR");
			model.addAttribute("error", "En este momento no se puede realizar la operación solicitada");
		}
		return returnPage;
	}

	@GetMapping("/cancel")
	public String cancel(@ModelAttribute("aplicacion") AplicacionModel aplicacion, Model model, SessionStatus status) {
		LOG.info("METHOD INVOKED FORM UI: cancel() -- PARAMS: " + aplicacion.toString());
		try {
			status.setComplete();
		} catch (Exception e) {
			LOG.error("Error al cancelar edicion de la aplicación", e);
			model.addAttribute("error", "En este momento no se puede realizar la operación solicitada");
		}
		return "redirect:/aplicaciones/main";
	}
}
