package com.jendrix.account.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@RequestMapping ("/invitaciones")
@SessionAttributes({"view"})
public class InvitacionesController {

	private static final Log LOG = LogFactory.getLog(InvitacionesController.class);
	
	@GetMapping("/main")
	public String main(Model model) {
		LOG.info("METHOD INVOKED FORM UI: main()");
		try {
			model.addAttribute("view", ViewNameConstant.INVITACION_MAIN);
		} catch (Exception e) {
			LOG.error("Error al cargar la página principal de invitaciones.", e);
			model.addAttribute("error", "No se puede cargar la página principal de invitaciones.");
		}

		return ViewNameConstant.INVITACION_MAIN.getName();
	}

}
