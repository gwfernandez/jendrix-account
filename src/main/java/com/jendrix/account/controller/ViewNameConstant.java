package com.jendrix.account.controller;

public enum ViewNameConstant {

	APLICACION_MAIN("aplicacion_main", "Aplicaciones", "APLICACIONES"), APLICACION_EDIT("aplicacion_edit", "Aplicaciones", "APLICACIONES"),
	DASHBOARD_MAIN("dashboard_main", "Dashboard", "DASHBOARD"),
	CUENTA_MAIN("cuenta_main", "Cuentas", "CUENTAS"),
	INVITACION_MAIN("invitacion_main", "Invitaciones", "INVITACIONES")
	;

	private String name;
	private String title; 
	private String menu;
	
	private ViewNameConstant(String name, String title, String menu) {
		this.name = name;
		this.title = title;
		this.menu = menu;
	}

	public String getName() {
		return name;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getMenu() {
		return menu;
	}
}
