package com.jendrix.account.controller;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jendrix.account.model.AplicacionModel;

@RestController
@RequestMapping("/rest")
public class AccountRestController {

	@GetMapping("checkRest")
	public ResponseEntity<AplicacionModel> checkRest() {

		AplicacionModel aplicacion = new AplicacionModel();
		aplicacion.setCodigo("JENDRIX_ACCOUNT");
		aplicacion.setDescripcion("Aplicación para administación de las cuentas de usuarios de Jendrix Software");
		aplicacion.setNombre("Jendrix Account");
		aplicacion.setVigenciaDesde(new Date());
		return new ResponseEntity<AplicacionModel>(aplicacion, HttpStatus.OK);
	}

}
