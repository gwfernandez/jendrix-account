package com.jendrix.account.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;

@Controller
// @SessionAttributes({ "userContext", "username" })
public class LoginController {

	// @Autowired
	// private UserContext userContext;

	private static final Log LOG = LogFactory.getLog(AplicacionController.class);

	@GetMapping("/login")
	public String showLogin(Model model, @RequestParam(name = "error", required = false) String error,
			@RequestParam(name = "logout", required = false) String logout, SessionStatus status) {
		model.addAttribute("error", error);
		model.addAttribute("logout", logout);

		status.setComplete();

		return "login";
	}

	@GetMapping({ "/loginSuccess", "/" })
	public String loginSuccess(Model model) {

		// // recuperar usuario de la session
		// model.addAttribute("username", userContext.getUsername());

		// LOG.info("LOGIN SUCCESS. Username " + userContext.getUsername());
		return "redirect:/dashboard/main";
	}

	@GetMapping("/logoutApp")
	public String logoutApp(HttpServletRequest request, HttpServletResponse response) {
		LOG.info("cerrar aplicacion:");

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "redirect:/login?logout";
	}

}
