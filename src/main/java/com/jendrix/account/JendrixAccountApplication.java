package com.jendrix.account;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class JendrixAccountApplication {

	public static void main(String[] args) {
		SpringApplication.run(JendrixAccountApplication.class, args);
	}
}
